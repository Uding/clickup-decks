import { ClassConstructor, ClassTransformOptions, plainToClass } from "class-transformer";
import { map } from 'rxjs/operators';

export const toClass = <T>(clazz: ClassConstructor<any>, options?: ClassTransformOptions) => map(plain => {
  return plainToClass(clazz, plain, options) as T;
});