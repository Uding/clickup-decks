
export interface DrawCardDTO {
  success: boolean;
  cards: CardDTO[];
  deck_id: string;
  remaining: number;
}

export interface NewDeckDTO {
  success: boolean;
  deck_id: string;
  shuffled: boolean;
  remaining: number;
}

export interface CardDTO {
  image: string;
  value: string;
  suit: string;
  code: string;
}