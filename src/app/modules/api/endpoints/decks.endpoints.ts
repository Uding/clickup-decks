import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { DrawCardDTO, NewDeckDTO } from "../dto/decks.dto";

/**
 * Apis are hard coded. Ideally we would have an SDK that contained 
 * the endpoints to consume.
 * 
 * Additionally, we could leverage the environments.ts to set some of the urls
 * up.
 */
@Injectable({
  providedIn: 'root'
})
export class DecksEndpoints {

  constructor(
    private readonly http: HttpClient
  ) {}

  /**
   * Creates a new deck and returns it
   * @returns `NewDeckDTO`
   */
  newDeck() {
    return this.http.get<NewDeckDTO>('https://deckofcardsapi.com/api/deck/new/');
  }

  /**
   * Draws cards from a deck
   * @param deckId the id of the deck to draw from
   * @param count the number of cards to draw
   * @returns `DrawCardDTO`
   */
  drawCards(deckId: string, count: number = 2): Observable<DrawCardDTO> {
    return this.http.get<DrawCardDTO>(`https://deckofcardsapi.com/api/deck/${deckId}/draw/?count=${count}`);
    // .pipe(
    //   map(drawCardDto => drawCardDto.cards)
    // );
  }
}