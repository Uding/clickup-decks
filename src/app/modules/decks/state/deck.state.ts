import { Deck } from "./deck";

export interface DeckState {
  deck: Deck | null;
  error: Error | null;
  loading: boolean;
}

export const initialDeckState = (): DeckState => {
  return {
    deck: null,
    error: null,
    loading: false,
  };
}