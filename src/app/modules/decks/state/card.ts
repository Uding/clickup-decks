import { Expose } from "class-transformer";

export class Card {
  @Expose()
  image: string;
  @Expose()
  value: string;
  @Expose()
  suit: string;
  @Expose()
  code: string;

  constructor() {
    this.image = '';
    this.value = '';
    this.suit = '';
    this.code = '';
  }
}