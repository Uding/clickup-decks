import { Action, createReducer, on } from "@ngrx/store";
import * as DeckActions from './deck.actions';
import { DeckState, initialDeckState } from "./deck.state";

const reducer = createReducer(
  initialDeckState(),
  on(DeckActions.CreateDeck, (state: DeckState) => {
    return {
      ...state,
      loading: true,
      error: null,
    }
  }),
  on(DeckActions.DeckCreated, (state: DeckState, action) => {
    return {
      ...state,
      deck: action.deck,
      loading: false,
      error: null,
    }
  }),
  on(DeckActions.DrawCards, (state: DeckState) => {
    return {
      ...state,
      loading: true,
      error: null
    }
  }),
  on(DeckActions.CardsDrawn, (state: DeckState, action) => {

    return {
      ...state,
      deck: state.deck ? { ...state.deck, remaining: action.remaining, cards: state.deck?.cards ? [...state.deck.cards, ...action.cards] : [...action.cards] } : null,
      loading: false,
      error: null,
    };
  }),
  on(DeckActions.CreateDeckFailed, (state: DeckState, error: Error) => {
    return {
      ...state,
      loading: false,
      error
    };
  }),
  on(DeckActions.DrawCardFailed, (state: DeckState, error: Error) => {
    return {
      ...state,
      loading: false,
      error,
    };
  })
);

export function DecksReducer(state: DeckState | undefined, action: Action) {
  return reducer(state, action);
}