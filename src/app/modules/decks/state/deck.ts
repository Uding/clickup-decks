import { Expose } from "class-transformer";
import { Card } from "./card";

export class Deck {
  @Expose({name: 'deck_id'})
  id!: string;
  @Expose()
  cards?: Card[];
  @Expose()
  remaining!: number;
}