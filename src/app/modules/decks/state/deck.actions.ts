import { createAction, props } from "@ngrx/store";
import { Card } from "./card";
import { Deck } from "./deck";

export const CreateDeck = createAction('[Deck] - Create New Deck');
export const DeckCreated = createAction('[Deck] - New Deck Created', props<{deck: Deck}>());
export const CreateDeckFailed = createAction('[Deck] - Create New Deck - Error', props<Error>());
export const DrawCards = createAction('[Deck] - Draw Cards', props<{count: number}>());
export const CardsDrawn = createAction('[Deck] - Cards Drawn', props<{remaining: number, cards: Card[]}>());
export const DrawCardFailed = createAction('[Deck] - Draw Cards Failed', props<Error>());