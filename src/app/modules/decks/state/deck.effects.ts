import { Injectable } from "@angular/core";
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { Action, Store } from "@ngrx/store";
import { Observable, of } from "rxjs";
import { catchError, map, mergeMap, switchMap, take } from "rxjs/operators";
import { toClass } from "src/app/rxjs/operators/to-class";
import { DecksEndpoints } from "../../api/endpoints/decks.endpoints";
import { Deck } from "./deck";
import * as DeckActions from './deck.actions';
import { DeckState } from "./deck.state";

@Injectable()
export class DeckEffects {

  constructor(
    private readonly deckEndpoints: DecksEndpoints,
    private readonly action$: Actions,
    private readonly store: Store<{decks: DeckState}>
  ) {}

  CreateDeck$: Observable<Action> = createEffect(() =>
    this.action$.pipe(
      ofType(DeckActions.CreateDeck),
      mergeMap(action => {
        return this.deckEndpoints.newDeck()
        .pipe(
          toClass<Deck>(Deck, { excludeExtraneousValues: true }),
        )
      }),
      map(result => {
        return DeckActions.DeckCreated({deck: result});
      }),
      catchError(error => {
        return of(DeckActions.CreateDeckFailed(error));
      })
    )
  );

  DrawCards$: Observable<Action> = createEffect(() =>
    this.action$.pipe(
      ofType(DeckActions.DrawCards),
      switchMap(action => {
        return this.store.select(state => state.decks.deck)
        .pipe(
          take(1),
          map(deck => {
            return { deck, count: action.count }
          }),
        )
      }),
      switchMap(action => {
        const {deck, count} = action;

        return this.deckEndpoints.drawCards(deck!.id, count)
        .pipe(
          map(result => {
            return DeckActions.CardsDrawn({remaining: result.remaining, cards: result.cards});
          }),
          catchError(error => {
            return of(DeckActions.DrawCardFailed(error));
          }),
        )
      }),
      catchError(error => {
        return of(DeckActions.DrawCardFailed(error));
      })
    )
  );
}