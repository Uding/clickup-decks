import { NgModule } from "@angular/core";
import { StoreModule } from "@ngrx/store";
import { DecksReducer } from "./state/deck.reducers";
import { DecksListComponent } from './components/decks-list/decks-list.component';
import { CommonModule } from "@angular/common";
import { DragDropModule } from "@angular/cdk/drag-drop";
import { DatatableModule } from "../datatable/datatable.module";
import { EffectsModule } from "@ngrx/effects";
import { DeckEffects } from "./state/deck.effects";
import { FontAwesomeModule } from "@fortawesome/angular-fontawesome";

@NgModule({
  declarations: [
    DecksListComponent
  ],
  imports: [
    CommonModule,
    DragDropModule,
    DatatableModule,
    StoreModule.forFeature('decks', DecksReducer),
    EffectsModule.forFeature([DeckEffects]),
    FontAwesomeModule,
  ],
  exports: [
    DecksListComponent
  ]
})
export class DecksModule {}