import { Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import { DeckState } from '../../state/deck.state';
import * as DeckActions from '../../state/deck.actions';
import { faSpinner } from '@fortawesome/free-solid-svg-icons'
import { DatatableColumnDef } from '../../../datatable/datatable.constants';

@Component({
  selector: 'bu-decks-list',
  templateUrl: './decks-list.component.html',
  styleUrls: ['./decks-list.component.scss']
})
export class DecksListComponent implements OnInit {

  spinner = faSpinner; //loading indicator

  /**
   * Hard coded for this demo. could be retrieved from some settings API 
   */
  allColumns: DatatableColumnDef[] = [
    {
      field: 'value',
      title: 'Value'
    },
    {
      field: 'suit',
      title: 'Suit'
    },
    {
      field: 'code',
      title: 'Code'
    }
  ];

  deck$: Observable<DeckState>;

  constructor(
    private readonly store: Store<{decks: DeckState}>
  ) {
    this.deck$ = store.select(('decks'));
  }

  ngOnInit(): void {

    this.store.dispatch(DeckActions.CreateDeck());
  }

  drawCardsOnClick() {
    this.store.dispatch(DeckActions.DrawCards({count: 13}));
  }
}
