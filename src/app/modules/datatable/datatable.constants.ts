export interface DatatableColumnDef {
  field: string;
  title: string;
}