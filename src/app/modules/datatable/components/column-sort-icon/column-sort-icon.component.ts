import { Component } from '@angular/core';
import { faArrowDown, faArrowUp } from '@fortawesome/free-solid-svg-icons';
import { DatatableStore } from '../../state/datatable.store';

@Component({
  selector: 'bu-column-sort-icon',
  templateUrl: './column-sort-icon.component.html',
  styleUrls: ['./column-sort-icon.component.scss'],
  inputs: ['field: field']
})
export class DatatableColumnSortIconComponent {

  public field: string;
  public upIcon = faArrowUp;
  public downIcon = faArrowDown;

  sortDirection$ = this.datatableStore.select(state => state.sortDirection);
  sortField$ = this.datatableStore.select(state => state.sortField);

  constructor(
    public readonly datatableStore: DatatableStore
  ) {
    this.field = '';
  }
}
