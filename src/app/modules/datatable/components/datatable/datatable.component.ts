import { CdkDragDrop } from '@angular/cdk/drag-drop';
import { Component, ContentChild, Input } from '@angular/core';
import { DatatableColumnDef } from '../../datatable.constants';
import { DatatableHeaderDef } from '../../directives/datatable-header-template.directive';
import { DatatableRowDef } from '../../directives/datatable-row-template.directive';
import { DatatableStore } from '../../state/datatable.store';

@Component({
  selector: 'bu-datatable, table[buDatatable]',
  templateUrl: './datatable.component.html',
  styleUrls: ['./datatable.component.scss'],
  providers: [DatatableStore]
})
export class DatatableComponent {
  
  @Input()
  set values(newValues: any[] | undefined) {
    this.store.setValues(newValues);
    this.store.sort(); // sort if we received new values
  }

  @Input()
  set columns(newColumns: DatatableColumnDef[]) {
    this.store.setColumns(newColumns)
  }

  @ContentChild(DatatableHeaderDef)
  set headerDef(def: DatatableHeaderDef) {
    this.store.setHeaderDef(def);
  }
  @ContentChild(DatatableRowDef)
  set rowDef(def: DatatableRowDef) {
    this.store.setRowDef(def);
  }

  vm$ = this.store.vm$;

  constructor(
    private readonly store: DatatableStore
  ) { }

  /**
   * Simple sort function that sorts based on the passed in column
   * @param column the column that triggered the event
   */
  sortColumn(column: DatatableColumnDef | null) {
    this.store.sortColumn(column);
  }

  /**
   * Rearranges columns based on the event
   * @param event the `CdkDragDrop`
   */
  colDrop(event: CdkDragDrop<string[]>) {
    this.store.columnDrop(event);
  }

  /**
   * Simple filter function
   * 
   * Ideally we would use some sort of strategy pattern to cover different cases like contains, exact match, etc
   * @param event 
   */
  filter(event: any) {
    this.store.filter(event ? event.value : null);
  }
}
