import { CdkDragDrop } from "@angular/cdk/drag-drop";
import { Injectable, TemplateRef } from "@angular/core";
import { ComponentStore } from "@ngrx/component-store";
import * as _ from 'lodash';
import { combineLatest, Observable } from "rxjs";
import { filter, map, mergeMap, switchMap, take, tap } from "rxjs/operators";
import { DatatableColumnDef } from "../datatable.constants";
import { DatatableHeaderDef } from "../directives/datatable-header-template.directive";
import { DatatableRowDef } from "../directives/datatable-row-template.directive";

export interface DatatableState {
  values: any[];
  filteredValues: any[];
  columns: DatatableColumnDef[];
  sortDirection: number;
  headerDef?: DatatableHeaderDef;
  rowDef?: DatatableRowDef;
  headerTemplate?: TemplateRef<any>;
  rowTemplate?: TemplateRef<any>;
  sortField?: string;
}

@Injectable()
export class DatatableStore extends ComponentStore<DatatableState> {

  constructor() {
    super({
      values: [],
      filteredValues: [],
      columns: [],
      sortDirection: 1,
      headerDef: undefined,
    });
  }

  readonly setValues = this.updater((state, values: any[] | undefined) => ({
    ...state,
    values: _.isNil(values) ? [] : values,
    filteredValues: _.isNil(values) ? [] : values
  }));

  readonly setFilteredValues = this.updater((state, filteredValues: any[]) => ({
    ...state,
    filteredValues
  }));

  readonly setColumns = this.updater((state, columns: DatatableColumnDef[]) => ({
    ...state,
    columns,
  }));

  readonly setHeaderDef = this.updater((state, def: DatatableHeaderDef) => ({
    ...state,
    headerDef: def,
  }));

  readonly setRowDef = this.updater((state, def: DatatableRowDef) => ({
    ...state,
    rowDef: def,
  }));

  readonly setSortField = this.updater((state, sortField: string) => ({
    ...state,
    sortField,
  }));

  readonly setSortDirection = this.updater((state, sortDirection: number) => ({
    ...state,
    sortDirection
  }));

  private readonly values$: Observable<any[]> = this.select(state => state.values);
  private readonly filteredValues$: Observable<any[]> = this.select(state => state.filteredValues);
  private readonly columns$: Observable<DatatableColumnDef[]> = this.select(state => state.columns);
  private readonly headerDef$: Observable<DatatableHeaderDef | undefined> = this.select(state => state.headerDef);
  private readonly rowDef$: Observable<DatatableRowDef | undefined> = this.select(state => state.rowDef);
  private readonly sortDirection$: Observable<number> = this.select(state => state.sortDirection);
  private readonly sortField$: Observable<string | undefined> = this.select(state => state.sortField);

  readonly vm$ = this.select(
    this.values$,
    this.filteredValues$,
    this.columns$,
    this.headerDef$,
    this.rowDef$,
    (values, filteredValues, columns, headerDef, rowDef) => ({
      values,
      filteredValues,
      columns,
      headerDef,
      rowDef
    })
  );

  /**
   * Updates columns based on the `CdkDragDrop` object
   */
  readonly columnDrop = this.effect((event$: Observable<CdkDragDrop<string[]>>) => {
    return event$
      .pipe(
        mergeMap(dragDrop => {
          return this.columns$ // grabbing columnDefs for later use
            .pipe(
              map(columnDefs => {
                return {
                  event: dragDrop,
                  columns: columnDefs
                };
              })
            );
        }),
        tap(next => {
          const { event, columns } = next;
          const currentIndex = event.currentIndex;
          const previousIndex = event.previousIndex;

          if (currentIndex === previousIndex) {
            return; // no-op if dropped in the same place
          }

          const target = columns[previousIndex];
          const direction = currentIndex < previousIndex ? -1 : 1;

          // shuffling objects based on where the next column landed
          for (let i = previousIndex; i !== currentIndex; i += direction) {
            columns[i] = columns[i + direction];
          }

          columns[currentIndex] = target;
          this.setColumns(columns);
        })
      )
  });

  /**
   * Simple function that allows for a single column to be sorted
   */
  readonly sortColumn = this.effect((column$: Observable<DatatableColumnDef | null>) => {
    return column$
      .pipe(
        filter(colDef => !_.isNil(colDef)),
        mergeMap(columnDef => {
          return combineLatest([ //grab direction and field for later use
            this.sortDirection$.pipe(take(1)),
            this.sortField$.pipe(take(1))
          ])
            .pipe(
              map(next => {
                return {
                  columnDef,
                  sortDirection: next[0],
                  sortField: next[1]
                };
              })
            );
        }),
        tap(next => {
          const { columnDef, sortDirection, sortField } = next;
          const field = columnDef!.field;
          let newSortDirection = sortDirection;

          if (sortField === field) {
            newSortDirection = sortDirection * -1;
          } else {
            newSortDirection = 1;
          }

          this.setSortField(field);
          this.setSortDirection(newSortDirection);
          this.sort(); // finally, sort after field and direction have been set up
        })
      );
  });

  /**
   * Simple text filter across all rows and columns
   */
  readonly filter = this.effect((value$: Observable<string | null>) => {
    return value$
      .pipe(
        mergeMap(filterValue => {
          return combineLatest([ // grabbing values and columns for later use
            this.values$.pipe(take(1)),
            this.columns$.pipe(take(1))
          ])
            .pipe(
              map(next => {
                return {
                  values: next[0],
                  columns: next[1],
                  filterValue,
                };
              })
            )
        }),
        tap(next => {
          const { values, columns, filterValue } = next;
          if (filterValue) {
            const trimmedFilterValue = filterValue.trim().toLowerCase();
            const filteredValues: any[] = [];

            /**
              * iterate through all values and all columns
              * we're looking to see if a property value matches the 
              * search value
            */
            for (let i = 0; i < values.length; i++) {
              const rowData = values[i];
              for (let col = 0; col < columns.length; col++) {
                const column = columns[col];

                // _.get allows us access to nested properties. For example: user.role.name
                const propertyValue = _.get(rowData, column.field).toLowerCase();

                if (propertyValue.indexOf(trimmedFilterValue) !== -1) {
                  filteredValues.push(rowData);
                  break;
                }
              }
            }

            this.setFilteredValues(filteredValues);
          } else {
            this.setFilteredValues(values);
          }
        })
      );
  })

  /**
   * Hidden sort function that uses the `sortField` and `sortDirection`
   */
  readonly sort = this.effect((origin$: Observable<void>) => {
    return origin$
      .pipe(
        switchMap(() => {
          return combineLatest([
            this.sortField$.pipe(take(1)),
            this.sortDirection$.pipe(take(1)),
            this.filteredValues$.pipe(take(1)),
          ]);
        }),
        tap(next => {
          const sortField = next[0];
          const sortDirection = next[1];
          const filteredValues = next[2];

          if (!_.isNil(sortField) && !_.isNil(sortDirection)) {
            const sortedValues = _.orderBy(filteredValues, sortField, sortDirection === 1 ? 'asc' : 'desc');
            this.setFilteredValues(sortedValues);
          }
        })
      )
  });
}