import { Directive, TemplateRef } from "@angular/core";

@Directive({selector: '[buHeaderDef]'})
export class DatatableHeaderDef {

  constructor(
    public templateRef: TemplateRef<any>
    ) {}
}