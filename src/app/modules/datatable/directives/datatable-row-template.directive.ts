import { Directive, TemplateRef } from "@angular/core";

@Directive({
  selector: '[buRowDef]'
})
export class DatatableRowDef {
  
  constructor(
    public templateRef: TemplateRef<any>
  ) {}
}