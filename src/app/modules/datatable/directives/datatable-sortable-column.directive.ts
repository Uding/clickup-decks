import { Directive, HostListener } from "@angular/core";
import { DatatableComponent } from "../components/datatable/datatable.component";
import { DatatableColumnDef } from "../datatable.constants";

/**
 * Directive that can be attached to a <th> to allow sorting for the column
 */
@Directive({
  selector: 'th[buSortableColumn]',
  inputs: ['column: buSortableColumn'],
  host: {
    'class': 'bu-sortable-column'
  }
})
export class DatatableSortableColumn {

  column: DatatableColumnDef | null;

  constructor(
    private dataTable: DatatableComponent
  ) {
    this.column = null;
  }

  @HostListener('click', ['$event'])
  onClick(ev: MouseEvent) {
    this.dataTable.sortColumn(this.column);
  }
}