import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { DatatableComponent } from './components/datatable/datatable.component';
import { DatatableHeaderDef } from "./directives/datatable-header-template.directive";
import { DatatableRowDef } from "./directives/datatable-row-template.directive";
import { DatatableSortableColumn } from "./directives/datatable-sortable-column.directive";
import { DatatableColumnSortIconComponent } from './components/column-sort-icon/column-sort-icon.component';
import { FontAwesomeModule } from "@fortawesome/angular-fontawesome";

@NgModule({

  declarations: [
    DatatableComponent,
    DatatableHeaderDef,
    DatatableRowDef,
    DatatableSortableColumn,
    DatatableColumnSortIconComponent,
  ],
  imports: [
    CommonModule,
    FontAwesomeModule,
  ],
  exports: [
    DatatableComponent,
    DatatableHeaderDef,
    DatatableRowDef,
    DatatableSortableColumn,
    DatatableColumnSortIconComponent,
  ]
})
export class DatatableModule {}