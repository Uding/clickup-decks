## Platform
Platform is a simple tool to draw cards from a deck using the following API:
http://deckofcardsapi.com/

## Features
### Features Include:
- Sort by suit and value
- Quick search
- Draw one suit at a time from the deck

### Error Handling
Error handling is in place for situations where the deck or cards cannot be retrieved. (It's a free API and 500's do happen)

## Install Depenencies
Run `npm i` to install the latest packages

## Development server
Run `ng serve` for a dev server.

## Project Structure

|-- src
|   |-- app
|       |-- modules
|       |   |-- api               # Module containging data transfer objects and root level endpoints
|       |   |   |-- dto           
|       |   |   |-- endpoints     
|       |   |
|       |   |-- datatable         # Module containing all components, directives, and services to support a reusable datatable
|       |   |   |-- components
|       |   |   |-- directives
|       |   |   |-- state
|       |   |
|       |   |-- decks             # Module containing all components and services to support a resuable deck list view
|       |       |-- components
|       |       |-- state
|       |
|       |-- rxjs                  # custom RxJs operators
|
|
...
